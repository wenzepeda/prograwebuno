<?php

// php select option value from database

$hostname = "localhost";
$username = "root";
$password = "";
$databaseName = "matricula_db";

// connect to mysql database

$connect = mysqli_connect($hostname, $username, $password, $databaseName);

// mysql select query
$query = "SELECT * FROM `carrera`";

// for method 1

$result1 = mysqli_query($connect, $query);


?>
<!DOCTYPE html>
<html>

<head>
    <title>MATRICULA</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
    <form action="signup-check.php" method="post">
        <h2>Matricula</h2>
        <?php if (isset($_GET['error'])) { ?>
            <p class="error"><?php echo $_GET['error']; ?></p>
        <?php } ?>

        <?php if (isset($_GET['success'])) { ?>
            <p class="success"><?php echo $_GET['success']; ?></p>
        <?php } ?>

        <label>Cédula</label>
        <?php if (isset($_GET['cedula'])) { ?>
            <input type="text" name="cedula" placeholder="X XXXX XXXX" value="<?php echo $_GET['cedula']; ?>"><br>
        <?php } else { ?>
            <input type="text" name="cedula" placeholder="X XXXX XXXX"><br>
        <?php } ?>

        <label>Nombre</label>
        <?php if (isset($_GET['nombre'])) { ?>
            <input type="text" name="nombre" placeholder="Nombre" value="<?php echo $_GET['nombre']; ?>"><br>
        <?php } else { ?>
            <input type="text" name="nombre" placeholder="Nombre"><br>
        <?php } ?>

        <label>Apellidos</label>
        <?php if (isset($_GET['apellidos'])) { ?>
            <input type="text" name="apellidos" placeholder="Apellidos" value="<?php echo $_GET['apellidos']; ?>"><br>
        <?php } else { ?>
            <input type="text" name="apellidos" placeholder="Apellidos"><br>
        <?php } ?>

        <label>Email</label>
        <?php if (isset($_GET['email'])) { ?>
            <input type="text" name="email" placeholder="Email" value="<?php echo $_GET['email']; ?>"><br>
        <?php } else { ?>
            <input type="text" name="email" placeholder="Email"><br>
        <?php } ?>

        <!--Method One-->
        <label>Carrera</label>
        <select>
            
            <?php while ($row1 = mysqli_fetch_array($result1)) :; ?>

                <option value="<?php echo $row1[0]; ?>"><?php echo $row1[2]; ?></option>

            <?php endwhile; ?>

        </select>
    </form>
</body>

</html>